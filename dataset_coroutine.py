import os

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


SUFFIXES = {"png", "jpg", "jpeg", "JPEG", "JPG"}


def image_coroutine(link):
    links = sorted([l for l in os.listdir(link) if l.split(".")[-1] in SUFFIXES])
    for i, img_name in enumerate(links):
        image_path = os.path.join(link, img_name)

        img = Image.open(image_path).convert("RGB")
        img = np.asarray(img)

        yield img


def crop_coroutine(img, size=256):
    rows, cols, _ = img.shape
    rows, cols = rows // 256, cols // 256

    for row in range(rows):
        for col in range(cols):
            crop = img[size * row : size * (row + 1), size * col : size * (col + 1)]

            yield crop


def one_dataset_coroutine(link, n=np.inf, size=256):
    number = 0
    img_cor = image_coroutine(link)
    for img in img_cor:
        crop_cor = crop_coroutine(img, size)
        for crop in crop_cor:
            number += 1
            yield crop
            if number == n:
                return number

    return number


def dataset_coroutine(links_list, each_n=None, total_n=np.inf, size=256):
    number = 0
    ns_list = []
    if isinstance(each_n, (int, float)):
        ns_list = [each_n] * len(links_list)
    if isinstance(each_n, list):
        assert len(each_n) == len(links_list)
        ns_list = each_n

    for ni, link in zip(ns_list, links_list):
        ni = min(ni, total_n)
        odc = one_dataset_coroutine(link, ni, size)
        odc_number = yield from odc
        
        print('odc_number: ', odc_number)
        
        number += odc_number
        total_n -= odc_number
        if total_n == 0:
            break

    print('total_number: ', number)
    return number