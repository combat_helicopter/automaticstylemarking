import numpy as np
import matplotlib.pyplot as plt


def draw_box(box, color, pass_if_inf=True):    
    if pass_if_inf:
        if (box['l'] == -np.inf) | (box['r'] == np.inf):
            return
        if (box['d'] == -np.inf) | (box['d'] == np.inf):
            return
        
    plt.plot([box['l'], box['r']], [box['d'], box['d']], color=color)
    plt.plot([box['l'], box['r']], [box['u'], box['u']], color=color)
    plt.plot([box['l'], box['l']], [box['d'], box['u']], color=color)
    plt.plot([box['r'], box['r']], [box['d'], box['u']], color=color)

    
def draw_clusters(umap_hists, labels):
    alpha=1
    s=0.5

    plt.figure(figsize=(10, 10))
    plt.scatter(umap_hists[0], umap_hists[1], c=labels, alpha=alpha, s=s);


    box_11 = {'l':-np.inf, 'r':np.inf, 'd':-10.5, 'u':-2.5}
    plt.plot([6.5, 15], [-2.5, -2.5], color='black', linestyle='--')

    box_10 = {'l':-np.inf, 'r':np.inf, 'd':-2.5, 'u':np.inf}
    plt.plot([6.5, 15], [-2.5, -2.5], color='black', linestyle='--')

    box_9 = {'l':-np.inf, 'r':np.inf, 'd':-np.inf, 'u':-10.5}
    plt.plot([6.5, 20], [-10.5, -10.5], color='black', linestyle='--')

    box_8 = {'l':-np.inf, 'r':0.5, 'd':-np.inf, 'u':np.inf}
    plt.plot([0.5, 0.5], [-1.5, 10], color='black', linestyle='--')
    plt.plot([0.5, 0.5], [-13.5, -20], color='black', linestyle='--')

    box_1 = {'l':-2.3, 'r':0.5, 'd':-7.25, 'u':-1.5}
    draw_box(box_1, color='red')

    box_2 = {'l':-1.3, 'r':4, 'd':-13.5, 'u':-7.25}
    draw_box(box_2, color='blue')

    box_3 = {'l':0.5, 'r':6.5, 'd':-7.25, 'u':-2.5}
    draw_box(box_3, color='orange')

    box_4 = {'l':4, 'r':6.5, 'd':-12, 'u':-7.25}
    draw_box(box_4, color='green')

    box_5 = {'l':3, 'r':5.6, 'd':-17.5, 'u':-14.7}
    draw_box(box_5, color='pink')

    box_6 = {'l':15, 'r':20, 'd':-6.1, 'u':4}
    draw_box(box_6, color='violet')

    box_7 = {'l':2.2, 'r':10, 'd':1, 'u':8}
    draw_box(box_7, color='gold')

    plt.axis('equal');
    plt.xlim([-10, 20])
    plt.ylim([-20, 10])

    plt.show()
