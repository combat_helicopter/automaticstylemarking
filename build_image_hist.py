import numpy as np

def get_img_hist(img, bins=256):
    r_hist = np.histogram(img[:, :, 0], bins=np.arange(0, 256, 1), density=True)[0]
    g_hist = np.histogram(img[:, :, 1], bins=np.arange(0, 256, 1), density=True)[0]
    b_hist = np.histogram(img[:, :, 2], bins=np.arange(0, 256, 1), density=True)[0]

    return np.concatenate([r_hist, g_hist, b_hist]) / 3


def get_color_histograms_of_images(coroutine, with_images=False):
    if with_images:
        imgs = []
    hists = []
    for i, crop in enumerate(coroutine):
        if with_images:
            imgs.append(crop)
        hists.append(get_img_hist(crop))

    hists = np.array(hists)
    if with_images:
        imgs = np.concatenate([img[None] for img in imgs], axis=0)
        return hists, imgs
    else:
        return hists


def color_hist_to_2d(hists, reducer):
    umap_hists = reducer.transform(hists).T
    umap_hists[1] *= -1
    return umap_hists