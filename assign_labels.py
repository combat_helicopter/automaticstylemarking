import pickle
import numpy as np


def load_reducer(umap_path='./umap/umap.file'):
    reducer = pickle.load((open(umap_path, 'rb')))
    return reducer


def check_in_box(umap_hists, box):
    x_mask = (box['l'] <= umap_hists[0]) & (umap_hists[0] < box['r'])
    y_mask = (box['d'] <= umap_hists[1]) & (umap_hists[1] < box['u'])
    return x_mask & y_mask


def assign_labels(umap_hists):
    boxes = {
        11: {'l':-np.inf, 'r':np.inf, 'd':-10.5, 'u':-2.5},
        10: {'l':-np.inf, 'r':np.inf, 'd':-2.5, 'u':np.inf},
        9: {'l':-np.inf, 'r':np.inf, 'd':-np.inf, 'u':-10.5},
        8: {'l':-np.inf, 'r':0.5, 'd':-np.inf, 'u':np.inf},
        7: {'l':2.2, 'r':10, 'd':1, 'u':8},
        6: {'l':15, 'r':20, 'd':-6.1, 'u':4},
        5: {'l':3, 'r':5.6, 'd':-17.5, 'u':-14.7},
        4: {'l':4, 'r':6.5, 'd':-12, 'u':-7.25},
        3: {'l':0.5, 'r':6.5, 'd':-7.25, 'u':-2.5},
        2: {'l':-1.3, 'r':4, 'd':-13.5, 'u':-7.25},
        1: {'l':-2.3, 'r':0.5, 'd':-7.25, 'u':-1.5}
    }
    
    labels = np.zeros(umap_hists.shape[1], dtype=int)
    boxes_line = sorted(list(boxes.items()), reverse=True)
    for label, box in boxes_line:
        box_mask = check_in_box(umap_hists, box)
        labels[box_mask] = label
    return labels